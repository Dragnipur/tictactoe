import java.util.Scanner;

public class GameLogic {
	private char[][] board;
	final private char EMPTY_CELL = '-';
	private Player[] players;
	private int playerNr = 1; // Start at 1 since we don't want to label a player "Player 0"
	private String markers = "xo+*<>?!&%abcdefghijklmnpqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	final private int MAX_PLAYERS = 60; // Number of different markers
	private int turn = 0;
	private UserInteraction userInteractionProxy = new UserInteraction();
	private Scanner userInput = new Scanner(System.in);
	private GameInitiation setUpGame = new GameInitiation(this);

	public char[][] getBoard() {
		char[][] boardClone = new char[board.length][];
		for (int i = 0; i < board.length; i++) {
			boardClone[i] = board[i].clone();
		}
		return boardClone;
	}

	public int getBoardSize() {
		return board.length;
	}

	public char getEMPTY_CELL() {
		return EMPTY_CELL;
	}

	public String getMarkers() {
		return markers;
	}

	public void setMarkers(String markers) {
		this.markers = markers;
	}

	public int getMAX_PLAYERS() {
		return MAX_PLAYERS;
	}

	public Scanner getScanner() {
		return userInput;
	}

	public UserInteraction getUserInteraction() {
		return userInteractionProxy;
	}

	public void runGame() {
		initializeGame();

		int maxTurns = (board.length * board.length) - 1; // Turn 0 is the first turn, so we have to subtract 1.
		Player winner = null;
		boolean isDraw = false;

		while (winner == null && !isDraw) {
			Player activePlayer = players[turn % players.length];
			Move nextMove = activePlayer.makeMove();
			setField(activePlayer, nextMove);
			drawBoard();
			winner = checkWin();
			if (turn == maxTurns && winner == null) {
				isDraw = true;
			}
			turn++;
		}
		endGame(isDraw, winner);
	}

	private void initializeGame() {
		buildBoard(setUpGame.askBoardSize());

		int nrPlayers = setUpGame.askNumberPlayers();
		boolean customMarkers = setUpGame.askIfCustomMarkers();
		createPlayers(nrPlayers, customMarkers);

		drawBoard();
	}

	private void buildBoard(int size) {
		board = new char[size][size];
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				board[i][j] = EMPTY_CELL;
			}
		}
	}

	private void createPlayers(int nrPlayers, boolean customMarkers) {
		players = new Player[nrPlayers];
		if (customMarkers) {
			setUpGame.askForCustomMarkers(nrPlayers);
		}
		createPlayersFromMarkers(markers.substring(0, nrPlayers));
	}

	private void createPlayersFromMarkers(String markersAdjustedLenght) {
		for (int i = 0; i < markersAdjustedLenght.length(); i++) {
			players[i] = new Human(this, playerNr, markersAdjustedLenght.charAt(i));
			playerNr++;
		}
	}

	public void drawBoard() {
		System.out.print("  ");
		for (int i = 0; i < board.length; i++) {
			System.out.print((i + 1) + " ");
		}
		System.out.println();
		for (int i = 0; i < board.length; i++) {
			System.out.print((i + 1) + " ");
			for (int j = 0; j < board[i].length; j++) {
				// TODO Increase whitespace for big boards to match with column numbers
				System.out.print(board[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}

	public void setField(Player activePlayer, Move nextMove) {
		board[nextMove.getRow()][nextMove.getColumn()] = activePlayer.getMarker();
	}

	public Player checkWin() {
		Player winner = checkHorizontalWin();
		if (winner == null) {
			winner = checkVerticalWin();
		}
		if (winner == null) {
			winner = checkDiagonalWinLeftToRight();
		}
		if (winner == null) {
			winner = ckeckDiagonalWinRightToLeft();
		}
		return winner;
	}

	private Player checkHorizontalWin() {
		boolean markersMatch = true;
		boolean searching = true;
		char firstMarker = ' ';
		char nextMarker = ' ';

		int row = 0;
		int column = 0;
		while (searching && (row < board.length)) {
			column = 0;
			firstMarker = board[row][column];
			if (firstMarker != EMPTY_CELL) {
				markersMatch = true;

				while (markersMatch && (column < (board[row].length) - 1)) {
					column++;
					nextMarker = board[row][column];
					if (firstMarker != nextMarker) {
						markersMatch = false;
					}
				}
				if (markersMatch == true) {
					searching = false;
					return getWinnerFromMarker(firstMarker);
				}
			}
			row++;
		}
		return null;
	}

	private Player checkVerticalWin() {
		boolean markersMatch = true;
		boolean searching = true;
		char firstMarker = ' ';
		char nextMarker = ' ';

		int row = 0;
		int column = 0;
		while (searching && (column < board[0].length)) {
			row = 0;
			firstMarker = board[row][column];
			if (firstMarker != EMPTY_CELL) {
				markersMatch = true;

				while (markersMatch && (row < board.length - 1)) {
					row++;
					nextMarker = board[row][column];
					if (firstMarker != nextMarker) {
						markersMatch = false;
					}
				}
				if (markersMatch == true) {
					searching = false;
					return getWinnerFromMarker(firstMarker);
				}
			}
			column++;
		}
		return null;
	}

	private Player checkDiagonalWinLeftToRight() {
		boolean markersMatch = true;
		char firstMarker = board[0][0];
		char nextMarker = ' ';

		if (firstMarker != EMPTY_CELL) {
			for (int i = 1; i < board.length; i++) {
				nextMarker = board[i][i];
				if (firstMarker != nextMarker) {
					markersMatch = false;
				}
			}
			if (markersMatch == true) {
				return getWinnerFromMarker(firstMarker);
			}
		}
		return null;
	}

	private Player ckeckDiagonalWinRightToLeft() {
		boolean markersMatch = true;
		char firstMarker = board[0][board.length - 1];
		char nextMarker = ' ';

		int row = 0;
		if (firstMarker != EMPTY_CELL) {
			for (int column = board.length - 2; column >= 0; column--) {
				row++;
				nextMarker = board[row][column];
				if (firstMarker != nextMarker) {
					markersMatch = false;
				}
			}
			if (markersMatch == true) {
				return getWinnerFromMarker(firstMarker);
			}
		}
		return null;
	}

	private Player getWinnerFromMarker(char marker) {
		int i = 0;
		while (players[i].getMarker() != marker) {
			i++;
		}
		return players[i];
	}

	private void endGame(boolean isDraw, Player winner) {
		if (isDraw) {
			displayDrawMessage();
		} else {
			displayWinnerMessage(winner);
		}
	}

	private void displayDrawMessage() {
		userInteractionProxy.printFromKey("textDraw");
	}

	private void displayWinnerMessage(Player winner) {
		String playerNumberText = Integer.toString(winner.getPlayerNr());
		String playerMarkerText = Character.toString(winner.getMarker());
		userInteractionProxy.printFromKey("textWinner", playerMarkerText, playerNumberText, playerMarkerText);
	}

}