import java.util.Scanner;

public class GameInitiation {
	private Scanner userInput;
	private UserInteraction userInteractionProxy;
	private GameLogic gameLogic;
	final private char YES = 'y';
	final private char NO = 'n';
	private StringBuilder customMarkerBuilder = new StringBuilder("");

	GameInitiation(GameLogic gameLogic) {
		this.gameLogic = gameLogic;
		userInput = gameLogic.getScanner();
		userInteractionProxy = gameLogic.getUserInteraction();
	}

	public int askBoardSize() {
		userInteractionProxy.printFromKey("textBoardSize");
		int size = checkInputInt();
		while (size <= 0) {
			userInteractionProxy.printFromKey("textInvalidNumber");
			size = checkInputInt();
		}
		return size;
	}

	private int checkInputInt() {
		try {
			return userInput.nextInt();
		} catch (java.util.InputMismatchException e) {
			return -1;
		} finally {
			userInput.nextLine();
		}
	}

	public int askNumberPlayers() {
		userInteractionProxy.printFromKey("textNumberPlayers", Integer.toString(gameLogic.getMAX_PLAYERS()));
		int number = checkInputInt();
		while (NumberOfPlayersOutOfBound(number)) {
			userInteractionProxy.printFromKey("textInvalidNumber");
			number = checkInputInt();
		}
		return number;
	}

	private boolean NumberOfPlayersOutOfBound(int number) {
		if (number <= 1 || number > gameLogic.getMAX_PLAYERS()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean askIfCustomMarkers() {
		userInteractionProxy.printFromKey("textCustomMarkers");
		char customMarkers = getInputLowercase();
		while (customMarkers != YES && customMarkers != NO) {
			userInteractionProxy.printFromKey("textInvalidCharacter");
			customMarkers = getInputLowercase();
			userInput.nextLine();
		}
		return customMarkers == YES;
	}

	private char getInputLowercase() {
		return Character.toLowerCase(userInput.next().charAt(0));
	}

	public void askForCustomMarkers(int nrPlayers) {
		for (int playerNr = 1; playerNr <= nrPlayers; playerNr++) {
			getPlayerCustomMarker(playerNr);
		}
		gameLogic.setMarkers(customMarkerBuilder.toString());
	}

	private void getPlayerCustomMarker(int playerNr) {
		userInteractionProxy.printFromKey("textEnterCustomMarker", Integer.toString(playerNr));
		char marker = getCustomMarkerInput();
		while (customMarkerBuilder.toString().indexOf(marker) >= 0) {
			userInteractionProxy.printFromKey("textMarkerTaken", customMarkerBuilder.toString());
			marker = getCustomMarkerInput();
		}
		customMarkerBuilder.append(marker);
		userInteractionProxy.printFromKey("textChosenMarker", Character.toString(marker));
	}

	private char getCustomMarkerInput() {
		userInput.nextLine();
		return getInputLowercase();
	}

}