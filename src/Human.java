import java.util.Scanner;

public class Human implements Player {
	private int playerNr;
	private char marker;
	private UserInteraction userInteractionProxy;
	private Scanner userInput;
	private char[][] board;
	private GameLogic gameLogic;

	public Human(GameLogic gameLogic, int playerNr, char marker) {
		this.marker = marker;
		this.playerNr = playerNr;
		this.gameLogic = gameLogic;
		this.userInteractionProxy = gameLogic.getUserInteraction();
		this.userInput = gameLogic.getScanner();
		inititate();
	}

	private void inititate() {
		int size = gameLogic.getBoardSize();
		board = new char[size][size];
		board = gameLogic.getBoard();
	}

	public int getPlayerNr() {
		return playerNr;
	}

	public char getMarker() {
		return marker;
	}

	public Move makeMove() {
		update();
		return getNextLegalMove();
	}

	private void update() {
		board = gameLogic.getBoard();
	}

	private Move getNextLegalMove() {
		userInteractionProxy.printFromKey("textMakeMove", Integer.toString(playerNr), Character.toString(marker));
		Move nextMove = getInputMove();

		boolean isLegalMove = false;
		while (!isLegalMove) {
			if (isOffBoard(nextMove)) {
				userInteractionProxy.printFromKey("textIvalidMoveNotOnBoard", Integer.toString(board.length));
				nextMove = getInputMove();
			} else if (!isBoardCellEmpty(nextMove)) {
				userInteractionProxy.printFromKey("textInvalidMoveCellOccupied");
				nextMove = getInputMove();
			} else {
				isLegalMove = true;
			}
		}
		return nextMove;
	}

	private Move getInputMove() {
		try {
			int row = convertUserInput(userInput.nextInt());
			int column = convertUserInput(userInput.nextInt());
			Move nextMove = new Move(row, column);
			return nextMove;
		} catch (java.util.InputMismatchException e) {
			return null;
		} finally {
			userInput.nextLine();
		}
	}

	private int convertUserInput(int input) {
		return input - 1;
	}

	private boolean isOffBoard(Move nextMove) {
		return nextMove == null
			|| nextMove.getRow() < 0
			|| nextMove.getRow() >= board.length
			|| nextMove.getColumn() < 0
			|| nextMove.getColumn() >= board.length;
	}

	private boolean isBoardCellEmpty(Move move) {
		return board[move.getRow()][move.getColumn()] == gameLogic.getEMPTY_CELL();
	}

}