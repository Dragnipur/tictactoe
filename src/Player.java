
public interface Player {

	int getPlayerNr();

	char getMarker();

	Move makeMove();

}