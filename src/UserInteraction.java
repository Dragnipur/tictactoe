
public class UserInteraction {
	private DisplayTextDictionary DisplayText = new DisplayTextDictionary();

	public void printFromKey(String DictionaryKey, String... placeholders) {
		System.out.println(String.format(getMessage(DictionaryKey), placeholders));
	}

	private String getMessage(String DictionaryKey) {
		String message = DisplayText.getValue(DictionaryKey);
		if (message != null) {
			return message;
		} else {
			return "String not found.";
		}
	}
	
}