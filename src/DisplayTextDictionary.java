import java.util.HashMap;
import java.util.Map;

public class DisplayTextDictionary {
	private Map<String, String> dictionary = new HashMap<String, String>();

	DisplayTextDictionary() {
		initializeDictionary();
	}

	public String getValue(String key) {
		return dictionary.get(key);
	}

	private void initializeDictionary() {
		dictionary.put("textBoardSize", "Please enter the size N of the board you would like to play on.\n"
			+ "(N x N board, recommended between 3-5.)"
			);
		dictionary.put("textChosenMarker", "Your chosen marker is: %s");
		dictionary.put("textCustomMarkers", "If you would like to customize your player markers, please enter \"Y\", "
			+ "otherwise, please enter \"N\".\n"
			+ "(\"Y\" for custom markers, \"N\" for default.)"
			);
		dictionary.put("textDraw", "It's a draw.");
		dictionary.put("textEnterCustomMarker", "Player %s:\n"
			+ "Please enter your marker.");
		dictionary.put("textInvalidCharacter", "Please enter a valid character.");
		dictionary.put("textInvalidMoveCellOccupied", "Please enter a valid move.\n"
			+ "(The cell has to be empty.)"
			);
		dictionary.put("textIvalidMoveNotOnBoard", "Please enter a valid move.\n"
			+ "(Number for row and column between 1 and %s.)"
			);
		dictionary.put("textInvalidNumber", "Please enter a valid number.");
		dictionary.put("textMakeMove", "Player %s: %s\n"
			+ "Please enter the number of the row and column where you would like to place your stone, seperated by "
			+ "a space.\n"
			+ "Row Column"
			);
		dictionary.put("textMarkerTaken", "This marker is already taken. Please enter a different one.\n"
			+ "(Markers taken so far: %s.)"
			);
		dictionary.put("textNumberPlayers", "Please enter the number M of players that will play.\n"
			+ "(M players, recommended 2, max. %s.)"
			);
		dictionary.put("textWinner", "%s Player %s wins! %s\n"
			+ "End.");
	}
}