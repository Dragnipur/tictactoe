
public class Computer implements Player {
	private int playerNr;
	private char marker;

	public Computer(int playerNr, char marker) {
		this.playerNr = playerNr;
		this.marker = marker;
	}

	public int getPlayerNr() {
		return playerNr;
	}

	public char getMarker() {
		return marker;
	}

	public Move makeMove() {
		// TODO Auto-generated method stub
		return null;
	}

}